using Avalonia;
using Avalonia.Controls;
using Avalonia.Data.Converters;
using Avalonia.Markup.Xaml;
using Avalonia.Media;
using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;

namespace AvaloniaConverterTest
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
#if DEBUG
            this.AttachDevTools();
#endif
            if (Directory.Exists("temp"))
            {
                System.IO.DirectoryInfo di = new DirectoryInfo("temp");

                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }
                foreach (DirectoryInfo dir in di.GetDirectories())
                {
                    dir.Delete(true);
                }
                // "fix" for race condition, sometimes the folder is deleted and not recreated
                Thread.Sleep(100);
            }
            Directory.CreateDirectory("temp");
            var vm = new MainWindowViewModel();
            vm.window = this;
            this.Find<Grid>("MainGrid").DataContext = vm;
            // clear the temporary files at launch

        }

        public void UpdateXAMLString()
        {
            var content = this.Find<Grid>("content");
            MainWindowViewModel vm = content.DataContext as MainWindowViewModel;
            
                content.Children.Clear();
                var rootObject = AvaloniaRuntimeXamlLoader.Parse(vm.XAMLData);

                if (rootObject is Grid g)
                {
                    if (g.Resources.ContainsKey("tempDrawingImage"))
                    {
                        if (g.Resources["tempDrawingImage"] is DrawingGroup dg)
                        {
                            if (dg.Children[0] is DrawingGroup dg2)
                            {
                                /*if (dg2.ClipGeometry != null)
                                {
                                    var bounds = dg2.ClipGeometry.Bounds;
                                    dg2.Children.Add(new GeometryDrawing(null, new Pen(Brushes.Black, 0.001), new RectangleGeometry(bounds)));
                                }*/
                            }
                        }
                    }
                }
                if (rootObject is DrawingGroup dg3)
                {
                    addDrawingGroup(dg3);
                }

                if (rootObject is ResourceDictionary d)
                {
                    foreach (object key in d.Keys)
                    {
                        if (d[key] is DrawingGroup dg2)
                        {
                            addDrawingGroup(dg2);
                        }
                    }
                }


            
        }
        void addDrawingGroup(DrawingGroup dg2)
        {
            var content = this.Find<Grid>("content");
            if (dg2.ClipGeometry == null)
            {
                if (dg2.Children.Count == 1 && dg2.Children[0] is DrawingGroup dg3 && dg3.ClipGeometry != null)
                {
                    addDrawingGroup(dg3);
                    return;
                }
            }
            if (dg2.ClipGeometry != null)
            {
                var bounds = dg2.ClipGeometry.Bounds;
                dg2.Children.Add(new GeometryDrawing() { Pen = new Pen(Brushes.Black, 0.001), Geometry = new RectangleGeometry(bounds) });
            }
            DrawingImage di = new DrawingImage();
            di.Drawing = dg2;
            Image im = new Image();
            im.Source = di;
            //content.Resources = d;
            content.Children.Add(im);
        }

        public void UpdateTargetImage()
        {
            var content = this.Find<Grid>("content");
            content.Children.Clear();
            MainWindowViewModel vm = content.DataContext as MainWindowViewModel;
            var image = vm.ConvertedImage;
            content.Children.Add(new Image() { Source = image });
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
    public class PathToFilenameConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Path.GetFileName(value.ToString());
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

}
