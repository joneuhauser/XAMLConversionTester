﻿using DynamicData;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.Emit;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.Scripting;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Reactive.Subjects;
using System.Reactive.Linq;
using System.Collections.ObjectModel;
using System.Reactive;
using System.Diagnostics;
using AvaloniaConverterTest.Properties;
using System.Text.RegularExpressions;

namespace AvaloniaConverterTest
{
    [Serializable]
    public enum ScriptType
    {
        Render,
        Convert,
    }
    [Serializable]
    public class ScriptObject : ReactiveObject
    {
        private string title;
        public string Title
        {
            get { return title; }
            set { this.RaiseAndSetIfChanged(ref title, value); }
        }

        private string SanitizedTitle
        {
            get
            {
                return Regex.Replace(Title, "\\W", "_");
            }
        }
        private string outputFiletype = ".png";
        public string OutputFiletype
        {
            get { return outputFiletype; }
            set => this.RaiseAndSetIfChanged(ref outputFiletype, value);
        }
        private string inputFiletype = ".svg";
        public string InputFiletype
        {
            get { return inputFiletype; }
            set => this.RaiseAndSetIfChanged(ref inputFiletype, value);
        }

        private string script = "public class Test \n { \n }";
        public string Script
        {
            get => script;
            set => this.RaiseAndSetIfChanged(ref script, value);
        }
        public ScriptType Type;

        public List<string> parameterNames = new() { "filename" };
        private Assembly assembly;

        private void Compile()
        {

            // define source code, then parse it (to the type used for compilation)
            SyntaxTree syntaxTree = CSharpSyntaxTree.ParseText(@"
                using System;
                using AvaloniaConverterTest;
                using System.IO;
                using System.Diagnostics;

                namespace AvaloniaConverterTest
                {
                    public class " + SanitizedTitle + @"
                    {"
                        + script +
                    @"}
                }");

            // define other necessary objects for compilation
            string assemblyName = Path.GetRandomFileName();
            var references = AppDomain.CurrentDomain.GetAssemblies()
                .Where(_ => !_.IsDynamic && !string.IsNullOrWhiteSpace(_.Location))
                .Select(_ => MetadataReference.CreateFromFile(_.Location))
                .Concat(new[]
                {
                    // add your app/lib specifics, e.g.:                      
                    MetadataReference.CreateFromFile(typeof(ReactiveObject).Assembly.Location),
                })
                .ToList();

            // analyse and generate IL code from syntax tree
            CSharpCompilation compilation = CSharpCompilation.Create(
                assemblyName,
                syntaxTrees: new[] { syntaxTree },
                references: references,
                options: new CSharpCompilationOptions(OutputKind.DynamicallyLinkedLibrary));

            using (var ms = new MemoryStream())
            {
                // write IL code into memory
                EmitResult result = compilation.Emit(ms);

                if (!result.Success)
                {
                    // handle exceptions
                    IEnumerable<Diagnostic> failures = result.Diagnostics.Where(diagnostic =>
                        diagnostic.IsWarningAsError ||
                        diagnostic.Severity == DiagnosticSeverity.Error);

                    foreach (Diagnostic diagnostic in failures)
                    {
                        System.Diagnostics.Debug.WriteLine("{0}: {1}", diagnostic.Id, diagnostic.GetMessage());
                    }
                }
                else
                {
                    // load this 'virtual' DLL so that we can use
                    ms.Seek(0, SeekOrigin.Begin);
                    assembly = Assembly.Load(ms.ToArray());
                }
            }
        }
        public static void RunProcess(string fileName, string args, Action<string> outputProcessing, Action endCallback)
        {

            var process = new Process
            {
                StartInfo =
                {
                    FileName = fileName, Arguments = args,
                    UseShellExecute = false, CreateNoWindow = true,
                    RedirectStandardOutput = true, RedirectStandardError = true,
                    
                },
                EnableRaisingEvents = true
            };
            outputProcessing(process.StartInfo.FileName + " " + process.StartInfo.Arguments + "\n");
            process.Exited += new EventHandler((o, ea) => {
                outputProcessing("Process exited");
                endCallback();
            });
            var dea = new DataReceivedEventHandler((o, ea) =>
            {
                if (ea.Data != null)
                {
                    outputProcessing(ea.Data + "\n");
                }
            });
            process.OutputDataReceived += dea;
            process.ErrorDataReceived += dea;

            process.Start();
            process.BeginOutputReadLine();
            process.BeginErrorReadLine();
        }

        public void Invoke(string source, string target, Action<string> message, Action onTermination, object[] args)
        {
            if (assembly == null)
            {
                Compile();
            }
            var allargs = new List<Object> { source, target, message, onTermination};
            allargs.AddRange(args);

            // create instance of the desired class and call the desired function
            Type type = assembly.GetType("AvaloniaConverterTest." + SanitizedTitle);
            object obj = Activator.CreateInstance(type);
            try
            {
                type.InvokeMember("Process",
                                BindingFlags.Default | BindingFlags.InvokeMethod,
                                null,
                                obj,
                                allargs.ToArray());
            }
            catch
            (Exception ex)
            {
                message(ex.ToString());
            }
        }
    }

    public class ScriptManager : ReactiveObject
    {
        private readonly SourceList<ScriptObject> scripts = new SourceList<ScriptObject>();

        public IObservable<IChangeSet<ScriptObject>> Connect() => scripts.Connect();

        private string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "AvaloniaConverterTest", "scripts.xml");
        public void Serialize()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(List<ScriptObject>));

            TextWriter writer = new StreamWriter(path);
            serializer.Serialize(writer, scripts.Items.ToList());
            writer.Close();
        }
        public void Deserialize()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(List<ScriptObject>));

            if (!File.Exists(path))
            {
                //this.AddScript(ScriptType.InputRender);
                //Serialize();
                File.Copy("Scripts.xml", path);
            }

            FileStream fs = new FileStream(path, FileMode.Open);
            var result = serializer.Deserialize(fs);
            scripts.Clear();
            scripts.AddRange((List<ScriptObject>)result);
            fs.Close();
        }
        public ScriptManager()
        {
            
            Deserialize();
        }
        /// <summary>
        /// Adds a Script of the specified style with a unique name, and immediately serializes.
        /// </summary>
        /// <param name="scripttype"></param>
        /// <returns></returns>
        public ScriptObject AddScript(ScriptType scripttype)
        {
            int appendix = 0;
            var getcurrentname = new Func<int, string>(i => i == 0 ? scripttype.ToString() : scripttype.ToString() + i);
            while (scripts.Items.Any(x => x.Title == getcurrentname(appendix)))
            {
                appendix += 1;
            }
            var result = new ScriptObject() { Title = getcurrentname(appendix), Type =  scripttype};
            scripts.Add(result);
            Serialize();
            return result;
        }
    }
    public class ScriptCategory : ReactiveObject
    {
        public ScriptType type;
    }
    public class ScriptsManagerViewModel : ReactiveObject
    {

        private ScriptType currentType = ScriptType.Render;
        public ScriptType CurrentType
        {
            get => currentType;
            set => this.RaiseAndSetIfChanged(ref currentType, value);
        }
        private readonly IObservable<Func<ScriptObject, bool>> _filter;

        private readonly ReadOnlyObservableCollection<ScriptObject> _items;
        public ReadOnlyObservableCollection<ScriptObject> Items => _items;

        private ScriptObject currentScript;
        public ScriptObject SelectedScript
        {
            get => currentScript;
            set => this.RaiseAndSetIfChanged(ref currentScript, value);
        }

        ReactiveCommand<Unit, Unit> AddScript { get; }

        public ScriptsManagerViewModel(ScriptManager manager)
        {

            _filter = this.WhenAnyValue(x => x.CurrentType, type => new Func<ScriptObject, bool>(x => x.Type == type));
            manager.Connect().Filter(_filter).ObserveOn(RxApp.MainThreadScheduler).Bind(out _items).Subscribe();

            AddScript = ReactiveCommand.Create(() => { SelectedScript = manager.AddScript(currentType);  });

        }

    }
}

