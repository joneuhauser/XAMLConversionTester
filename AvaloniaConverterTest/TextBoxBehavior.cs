﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.Primitives;
using Avalonia.Input;
using Avalonia.Interactivity;
using Avalonia.Media;
using Avalonia.VisualTree;
using Avalonia.Xaml.Interactivity;
using System;

namespace AvaloniaConverterTest
{
    public class ScrollTextBehavior : Behavior<TextBox>
    {
        private ScrollViewer _scrollViewer;

        public static bool GetIsSet(TextBox obj)
        {
            return obj.GetValue(IsSetProperty);
        }

        public static void SetIsSet(TextBox obj, bool value)
        {
            obj.SetValue(IsSetProperty, value);
        }

        public static readonly AttachedProperty<bool> IsSetProperty =
            AvaloniaProperty.RegisterAttached<ScrollTextBehavior, TextBox, bool>
            (
                "IsSet"
            );
        static ScrollTextBehavior()
        {
            
            IsSetProperty.Changed.Subscribe(OnIsSetChanged);
        }

        // set the PointerPressed handler when 
        private static void OnIsSetChanged(AvaloniaPropertyChangedEventArgs<bool> args)
        {
            Control control = (Control)args.Sender;

            if (args.NewValue.Value == true)
            {
                if (control is TextBox textBox)
                {
                    // connect the pointer pressed event handler
                    textBox?.GetObservable(TextBlock.TextProperty)?.Subscribe(_ => textBox?.FindDescendantOfType<ScrollViewer>()?.ScrollToEnd());
                }
            }
            else
            {
            }
        }
    }
}