﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Media.Imaging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Windows;
using AvaloniaConverterTest.Properties;
using Avalonia.Markup.Xaml;
using ReactiveUI;
using System.Reactive.Linq;
using DynamicData;
using System.Reactive.Subjects;
using Avalonia.Platform.Storage;
using Avalonia.Platform;
//using Avalonia.Imaging;

namespace AvaloniaConverterTest
{
    public class MainWindowViewModel : ReactiveObject
    {
        public MainWindow window;

        public string SourceFolder
        {
            get
            {
                string fn = Properties.Settings.Default.Filename;
                return Directory.Exists(fn) ? fn : Path.GetDirectoryName(fn);
            }
            set
            {
                this.RaisePropertyChanged();
            }
        }

        private ObservableCollection<string> _filesList;

        public ObservableCollection<string> FilesList
        {
            get { return _filesList; }
            set { this.RaiseAndSetIfChanged(ref _filesList, value); }
        }

        public string CurrentFileName
        {
            get { return FilesList[CurrentIndex]; }
        }

        private int _currentIndex;

        public int CurrentIndex
        {
            get { return _currentIndex; }
            set
            {
                if (_currentIndex == value)
                    return;
                _currentIndex = value;
                if (value < FilesList.Count && value >= 0)
                {
                    Properties.Settings.Default.Filename = FilesList[value];
                    Properties.Settings.Default.Save();
                    ReadFile();
                    this.RaisePropertyChanged();

                    this.RaisePropertyChanged("CurrentFileName");
                    this.RaisePropertyChanged("SourceFolder");
                }
            }
        }

        private Bitmap _originalImage;
        public Bitmap OriginalImage
        {
            get
            {
                return _originalImage;
            }
            set
            {
                this.RaiseAndSetIfChanged(ref _originalImage, value);
            }
        }
        private Bitmap _convertedImage;
        public Bitmap ConvertedImage
        {
            get
            {
                return _convertedImage;
            }
            set
            {
                this.RaiseAndSetIfChanged(ref _convertedImage, value);
                Avalonia.Threading.Dispatcher.UIThread.InvokeAsync(() => window.UpdateTargetImage());
            }
        }
        private string xamlData;

        public string XAMLData
        {
            get { return xamlData; }
            set
            {
                xamlData = value;
                updateXAML();
            }
        }
        private async void updateXAML()
        {
            try
            {
                await Avalonia.Threading.Dispatcher.UIThread.InvokeAsync(() => window.UpdateXAMLString());
            }

            catch (Exception ex)
            {
                OutputProcessingText += "\nError processing file: \n" + ex;
            }
        }

        private double scale = 1;

        public double Scale
        {
            get { return scale; }
            set
            {
                scale = value;
                if (scale / (currentSVGSize / 1000) > 3 && (int)(scale * 1000) < 20000)
                {
                    DrawInput(CurrentFileName, (int)(scale * 1000));
                    if (SelectedConverter?.OutputFiletype != ".xaml")
                    {
                        // Vector, no need to refresh
                        DrawOutput(ConvertedFile, (int)(scale * 1000));
                    }
                    
                }
                this.RaisePropertyChanged();
            }
        }
        private double opacity = 0.5;

        public double Opacity
        {
            get { return opacity; }
            set { this.RaiseAndSetIfChanged(ref opacity, value); }
        }

        private double moveX;

        public double MoveX
        {
            get { return moveX; }
            set { this.RaiseAndSetIfChanged(ref moveX, value); }
        }

        private double moveY;

        public double MoveY
        {
            get { return moveY; }
            set { this.RaiseAndSetIfChanged(ref moveY, value); }
        }
        private string inputProcessingText;

        public string InputProcessingText
        {
            get { return inputProcessingText; }
            set { this.RaiseAndSetIfChanged(ref inputProcessingText, value); }
        }

        private string outputProcessingText;

        public string OutputProcessingText
        {
            get { return outputProcessingText; }
            set { this.RaiseAndSetIfChanged(ref outputProcessingText, value); }
        }

        private bool outputRefreshing;

        public bool OutputRefreshing
        {
            get { return outputRefreshing; }
            set { this.RaiseAndSetIfChanged(ref outputRefreshing, value); }
        }
        private bool inputRefreshing;

        public bool InputRefreshing
        {
            get { return inputRefreshing; }
            set { this.RaiseAndSetIfChanged(ref inputRefreshing, value); }
        }


        private bool forceUpdate;

        public bool ForceUpdate
        {
            get { return forceUpdate; }
            set { this.RaiseAndSetIfChanged(ref forceUpdate, value); }
        }

        private ScriptManager scriptManager;


        private readonly ReadOnlyObservableCollection<ScriptObject> _inputRenderScripts;
        public ReadOnlyObservableCollection<ScriptObject> InputRenderScripts => _inputRenderScripts;

        private readonly ReadOnlyObservableCollection<ScriptObject> _outputRenderScripts;
        public ReadOnlyObservableCollection<ScriptObject> OutputRenderScripts => _outputRenderScripts;

        private readonly ReadOnlyObservableCollection<ScriptObject> _convertScripts;
        public ReadOnlyObservableCollection<ScriptObject> ConvertScripts => _convertScripts;

        private ScriptObject selectedInputRenderer;
        public ScriptObject SelectedInputRenderer
        {
            get { return selectedInputRenderer; }
            set { this.RaiseAndSetIfChanged(ref selectedInputRenderer, value); }
        }
        private ScriptObject selectedConverter;
        public ScriptObject SelectedConverter
        {
            get { return selectedConverter; }
            set
            {
                if (value != null && !ConvertScripts.Contains(value)) return;

                this.RaiseAndSetIfChanged(ref selectedConverter, value); System.Diagnostics.Debug.WriteLine(value == null ? value : value.Title);
            }
        }
        private ScriptObject selectedOutputRenderer;
        public ScriptObject SelectedOutputRenderer
        {
            get { return selectedOutputRenderer; }
            set { this.RaiseAndSetIfChanged(ref selectedOutputRenderer, value); }
        }


        private readonly ISubject<Func<ScriptObject, bool>> _converterFilter;
        private readonly ISubject<Func<ScriptObject, bool>> _outputFilter;
        public Window GetWindow()
        {
            if (Avalonia.Application.Current.ApplicationLifetime is Avalonia.Controls.ApplicationLifetimes.ClassicDesktopStyleApplicationLifetime d)
                return d.MainWindow;
            return null;
        }


        #region command defs
        private RelayCommand _openCommand;

        public RelayCommand OpenCommand
        {
            get { return _openCommand; }
            set { this.RaiseAndSetIfChanged(ref _openCommand, value); }
        }
        private RelayCommand _forwardCommand;

        public RelayCommand ForwardCommand
        {
            get { return _forwardCommand; }
            set { this.RaiseAndSetIfChanged(ref _forwardCommand, value); }
        }
        private RelayCommand _backwardsCommand;

        public RelayCommand BackwardsCommand
        {
            get { return _backwardsCommand; }
            set { this.RaiseAndSetIfChanged(ref _backwardsCommand, value); }
        }
        private RelayCommand _refreshCommand;

        public RelayCommand RefreshCommand
        {
            get { return _refreshCommand; }
            set { this.RaiseAndSetIfChanged(ref _refreshCommand, value); }
        }

        private RelayCommand _resetViewCommand;

        public RelayCommand ResetViewCommand
        {
            get { return _resetViewCommand; }
            set { this.RaiseAndSetIfChanged(ref _resetViewCommand, value); }
        }

        private RelayCommand _openFileCommand;

        public RelayCommand OpenFileCommand
        {
            get { return _openFileCommand; }
            set { _openFileCommand = value; }
        }

        private int currentSVGSize = 1000;

        #endregion
        public MainWindowViewModel()
        {
            Properties.Settings.Default.Upgrade();

            scriptManager = new ScriptManager();

            // Wire up the Script selectors
            scriptManager.Connect().Filter(x => x.Type == ScriptType.Render).ObserveOn(RxApp.MainThreadScheduler)
            .Bind(out _inputRenderScripts).Subscribe();
            _converterFilter = new BehaviorSubject<Func<ScriptObject, bool>>(x => true);
            _outputFilter = new BehaviorSubject<Func<ScriptObject, bool>>(x => true);

            this.WhenAnyValue(x => x.SelectedInputRenderer).Subscribe(x =>
            {
                _converterFilter.OnNext(y => y.InputFiletype == x?.InputFiletype);
                if (x != null)
                {
                    Settings.Default.InputRenderer = x.Title;
                    Settings.Default.Save();
                }

            });
            this.WhenAnyValue(x => x.SelectedConverter).Subscribe(x =>
            {
                _outputFilter.OnNext(y => y.InputFiletype == x?.OutputFiletype);
                if (x != null)
                {
                    Settings.Default.Converter = x.Title;
                    Settings.Default.Save();
                }
            });
            this.WhenAnyValue(x => x.SelectedOutputRenderer).Subscribe(x =>
            {
                if (x != null)
                {
                    Settings.Default.OutputRenderer = x.Title;
                    Settings.Default.Save();
                }
            });

            scriptManager.Connect().Filter(x => x.Type == ScriptType.Convert).Filter(_converterFilter)
                .Bind(out _convertScripts).ObserveOn(RxApp.MainThreadScheduler).Subscribe();
            scriptManager.Connect().Filter(x => x.Type == ScriptType.Render).Filter(_outputFilter)
                .Bind(out _outputRenderScripts).ObserveOn(RxApp.MainThreadScheduler).Subscribe();
            scriptManager.Deserialize();

            // Initialize the scripts from the settings
            SelectedInputRenderer = _inputRenderScripts.Where(x => x.Title == Settings.Default.InputRenderer).FirstOrDefault();
            SelectedConverter = _convertScripts.Where(x => x.Title == Settings.Default.Converter).FirstOrDefault();
            SelectedOutputRenderer = _outputRenderScripts.Where(x => x.Title == Settings.Default.OutputRenderer).FirstOrDefault();


            this.WhenAnyValue(x => x.SelectedInputRenderer, x => x.SelectedConverter, x => x.SelectedOutputRenderer).Subscribe(_ => ReloadFiles());

            _converterFilter.Delay(TimeSpan.FromMilliseconds(10)).Subscribe(x => // The delay is a hack to ensure that the list has been updated already
            {
                if (!_convertScripts.Contains(selectedConverter))
                    SelectedConverter = _convertScripts.FirstOrDefault();
            });
            _outputFilter.Delay(TimeSpan.FromMilliseconds(10)).Subscribe(x =>
            {
                if (!_outputRenderScripts.Contains(selectedOutputRenderer))
                    SelectedOutputRenderer = _outputRenderScripts.FirstOrDefault();
            });

            // TODO: Sensible defaults, reload from settings

            OpenCommand = new RelayCommand(async (o) =>
            {
                var options = new FolderPickerOpenOptions() { SuggestedStartLocation = await window.StorageProvider.TryGetFolderFromPathAsync(SourceFolder), AllowMultiple = false };
                var result = await window.StorageProvider.OpenFolderPickerAsync(options);
                if (result.Count != 0)
                {
                    var res = result.First().Path.LocalPath;
                    SourceFolder = res;
                    ReloadFiles(res);
                }

            });
            ForwardCommand = new RelayCommand((o) => CurrentIndex++, (o) => CurrentIndex < FilesList.Count());
            BackwardsCommand = new RelayCommand((o) => CurrentIndex--, (o) => CurrentIndex > 0);
            RefreshCommand = new RelayCommand((o) => ReloadFiles());
            ResetViewCommand = new RelayCommand((o) =>
            {
                ResetView();
                ForceUpdate = true;
            });
            OpenFileCommand = new RelayCommand((o) => OpenConvertedFile());
        }

        private void ResetView()
        {
            Scale = 1; MoveX = 0; MoveY = 0;
        }

        private void ReadFile()
        {
            OutputProcessingText = "";
            InputProcessingText = "";
            DrawInput(CurrentFileName);


            Convert(CurrentFileName);
        }
        private Bitmap ReadBitmap(string path)
        {

            if (File.Exists(path))
            {
                Bitmap src = new Bitmap(path);
                return src;
            }
            else
            {
                OutputProcessingText += "File not found";
                return null;
            }
        }
        private string DrawInput(string filename, int size = 0)
        {
            InputRefreshing = true;
            if (SelectedInputRenderer == null)
            {
                InputProcessingText += "Please select an input renderer";
                return "";
            }
            string outfn = Path.Combine("temp", Path.GetFileNameWithoutExtension(filename) + "_in_" + DateTime.Now.Ticks + SelectedInputRenderer.OutputFiletype);

            SelectedInputRenderer.Invoke(filename, outfn, new Action<string>((s) =>
            {
                InputProcessingText += s;
            }), () =>
            {
                OriginalImage = ReadBitmap(outfn);
                InputRefreshing = false;
                ForceUpdate = true;
            }, new object[] { size.ToString() });

            OriginalImage = ReadBitmap(outfn);
            InputRefreshing = false;
            ForceUpdate = true;

            currentSVGSize = size;
            return outfn;
        }
        private string ConvertedFile;
        private void Convert(string filename)
        {
            StopWatcher();
            if (SelectedConverter == null)
            {
                OutputProcessingText += "\nPlease select a converter.";
                return;
            }

            ConvertedFile = Path.Combine("temp", Path.GetFileNameWithoutExtension(filename) + "_conv_" + DateTime.Now.Ticks + SelectedConverter.OutputFiletype);
            OutputRefreshing = true;

            SelectedConverter.Invoke(filename, ConvertedFile, new Action<string>((s) =>
            {
                OutputProcessingText += s;
            }), () => { DrawOutput(ConvertedFile); StartFileSystemWatcher(ConvertedFile); }, new object[] { });
        }
        private void DrawOutput(string filename, int size = 0)
        {
            if (SelectedConverter?.OutputFiletype == ".xaml")
            {
                // This is a special case
                DrawXAML(ConvertedFile);
            }

            if (SelectedOutputRenderer == null)
            {
                OutputProcessingText += "Please select an input renderer";
                return;
            }
            string outfn = Path.Combine("temp", Path.GetFileNameWithoutExtension(filename) + "_out_" + DateTime.Now.Ticks + SelectedOutputRenderer.OutputFiletype);

            SelectedOutputRenderer.Invoke(ConvertedFile, outfn, new Action<string>((s) =>
            {
                OutputProcessingText += s;
            }), () =>
            {
                ConvertedImage = ReadBitmap(outfn);
                OutputRefreshing = false;
                ForceUpdate = true;
            }, new object[] { size.ToString() });
        }

        private void StopWatcher()
        {
            if (watcher != null)
            {
                watcher.Changed -= OnChanged;
            }
        }

        FileSystemWatcher watcher;
        private void StartFileSystemWatcher(string filename)
        {
            watcher = new FileSystemWatcher();
            watcher.Path = Path.GetDirectoryName(filename);
            watcher.NotifyFilter = NotifyFilters.LastWrite;
            watcher.Filter = Path.GetFileName(filename);
            watcher.Changed += OnChanged;
            watcher.EnableRaisingEvents = true;
        }

        private void OnChanged(object sender, FileSystemEventArgs e)
        {
            Thread.Sleep(100);
            DrawXAML(e.FullPath);
        }

        private void DrawXAML(string filename)
        {
            OutputRefreshing = true;

            try
            {
                StreamReader sr = new StreamReader(filename);
                XAMLData = sr.ReadToEnd();
                sr.Close();


                ResetView();
                OutputProcessingText += "\nDrawing complete\n";
            }
            catch (IOException ex)
            {
                OutputProcessingText += "\nCan't read file\n";
            }
            OutputRefreshing = false;
            ForceUpdate = true;

        }
        private void OpenConvertedFile()
        {
            RunProcess(Properties.Settings.Default.EditorPath, "\"" + ConvertedFile + "\"", (o) => { }, () => { });
        }

        private void ReloadFiles(string fn = null, string extension = null)
        {
            if (fn == null)
                fn = SourceFolder;
            if (extension == null)
                extension = SelectedInputRenderer?.InputFiletype ?? "";
            FilesList = new ObservableCollection<string>(Directory.EnumerateFiles(fn, "*" + extension).OrderBy(x => x));
            fn = Properties.Settings.Default.Filename;
            int tentative_index = FilesList.IndexOf(fn);
            if (File.Exists(fn) && tentative_index != -1)
                CurrentIndex = tentative_index;
            else
                CurrentIndex = 0;
        }

        public static void RunProcess(string fileName, string args, Action<string> outputProcessing, Action endCallback)
        {

            var process = new Process
            {
                StartInfo =
                {
                    FileName = fileName, Arguments = args,
                    UseShellExecute = false, CreateNoWindow = true,
                    RedirectStandardOutput = true, RedirectStandardError = true
                },
                EnableRaisingEvents = true
            };
            outputProcessing(process.StartInfo.FileName + " " + process.StartInfo.Arguments + "\n");
            process.Exited += new EventHandler((o, ea) =>
            {
                outputProcessing("Process exited");
                endCallback();
            });
            var dea = new DataReceivedEventHandler((o, ea) =>
            {
                if (ea.Data != null)
                {
                    outputProcessing(ea.Data + "\n");
                }
            });
            process.OutputDataReceived += dea;
            process.ErrorDataReceived += dea;

            process.Start();
            process.BeginOutputReadLine();
            process.BeginErrorReadLine();
        }
    }
}
