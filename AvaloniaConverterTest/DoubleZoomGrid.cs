﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Windows;
using Avalonia.Controls;
using System.Windows.Input;
using Avalonia.Media;
using Avalonia.Media.Imaging;
using Avalonia;
using Avalonia.Input;
using Avalonia.Platform;
using SkiaSharp;
using System.Reflection;

namespace AvaloniaConverterTest
{

    

    public class DoubleZoomGrid : Grid
    {
        public Bitmap ComparisonImage
        {
            get { return (Bitmap)GetValue(ComparisonImageProperty); }
            set { SetValue(ComparisonImageProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ComparisonImage.  This enables animation, styling, binding, etc...
        public static readonly AvaloniaProperty ComparisonImageProperty =
            AvaloniaProperty.Register<DoubleZoomGrid, Bitmap>("ComparisonImage", defaultBindingMode: Avalonia.Data.BindingMode.TwoWay, defaultValue: null);



        public double Scale
        {
            get { return (double)GetValue(ScaleProperty); }
            set { SetValue(ScaleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Scale.  This enables animation, styling, binding, etc...
        public static readonly StyledProperty<double> ScaleProperty =
            StyledProperty<double>.Register<DoubleZoomGrid, double>("Scale", defaultValue: 1, defaultBindingMode: Avalonia.Data.BindingMode.TwoWay);



        public double MoveX
        {
            get { return (double)GetValue(MoveXProperty); }
            set { SetValue(MoveXProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MoveX.  This enables animation, styling, binding, etc...
        public static readonly StyledProperty<double> MoveXProperty =
            StyledProperty<double>.Register<DoubleZoomGrid, double>("MoveX", defaultValue: 0, defaultBindingMode: Avalonia.Data.BindingMode.TwoWay);



        public double MoveY
        {
            get { return (double)GetValue(MoveYProperty); }
            set { SetValue(MoveYProperty, value); }
        }

        public object BitmapFactory { get; private set; }

        // Using a DependencyProperty as the backing store for MoveY.  This enables animation, styling, binding, etc...
        public static readonly StyledProperty<double> MoveYProperty =
            AvaloniaProperty.Register<DoubleZoomGrid, double>("MoveY", defaultValue: 0, defaultBindingMode: Avalonia.Data.BindingMode.TwoWay);

        // super ugly, forces the view to update itself after the render / conversion process finished
        public bool ForceUpdate
        {
            get { return (bool)GetValue(ForceUpdateProperty); }
            set { SetValue(ForceUpdateProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ForceUpdate.  This enables animation, styling, binding, etc...
        public static readonly StyledProperty<bool> ForceUpdateProperty =
            AvaloniaProperty.Register<DoubleZoomGrid, bool>("ForceUpdate", defaultValue: false, defaultBindingMode: Avalonia.Data.BindingMode.TwoWay);

        public DoubleZoomGrid()
        {
            PropertyChanged += OnDoubleZoomGridPropertyChanged;
        }

        private void OnDoubleZoomGridPropertyChanged(object sender, AvaloniaPropertyChangedEventArgs e)
        {
            if (e.Property == ForceUpdateProperty)
            {
                // Execute your callback here
                GetComparedImage();
                // ...
            }
        }

        private Point start;
        private Point origin;

        public override void EndInit()
        {
            this.PointerWheelChanged += DoubleZoomGrid_MouseWheel;
            this.PointerPressed += DoubleZoomGrid_PointerPressed;
            this.PointerMoved += DoubleZoomGrid_PointerMoved;
            this.PointerReleased += DoubleZoomGrid_PointerReleased;
            
        }



        private void DoubleZoomGrid_PointerPressed(object sender, PointerPressedEventArgs e)
        {
            if (e.GetCurrentPoint(this).Properties.PointerUpdateKind != PointerUpdateKind.LeftButtonPressed) return;
            foreach (var child in Children.OfType<Border>())
            {
                if (child.Child == null) continue;
                if (child.Child.IsPointerOver)
                {
                    start = e.GetPosition(this);
                    this.Cursor = new Cursor(StandardCursorType.Hand);
                    origin = new Point(MoveX, MoveY);
                    // e.Pointer.Capture(child);

                }
            }
        }

        public void RefreshComparison()
        {
            GetComparedImage();
        }
        private void DoubleZoomGrid_PointerReleased(object sender, PointerReleasedEventArgs e)
        {
            // this.ReleaseMouseCapture();
            this.Cursor = Cursor.Default;
        }

        private void DoubleZoomGrid_PointerMoved(object sender, PointerEventArgs e)
        {
            if (e.GetCurrentPoint(this).Properties.IsLeftButtonPressed)
            {
                Vector v = start - e.GetPosition(this);
                MoveX = origin.X - v.X;
                MoveY = origin.Y - v.Y;
                GetComparedImage();
            }
        }

        private void DoubleZoomGrid_MouseWheel(object sender, PointerWheelEventArgs e)
        {
            double zoom = e.Delta.Y > 0 ? 1.1 : 1d / (1.1);
            if (!(e.Delta.Y > 0) && (Scale < .4 || Scale < .4))
                return;

            foreach (var child in Children.OfType<Border>())
            {
                if (child.Child == null) continue;
                if (child.Child.IsPointerOver)
                {
                    Point relative = e.GetPosition(child.Child);
                    Console.WriteLine(relative);
                    double absoluteX;
                    double absoluteY;

                    absoluteX = relative.X * Scale + MoveX;
                    absoluteY = relative.Y * Scale + MoveY;

                    Scale *= zoom;

                    MoveX = absoluteX - relative.X * Scale;
                    MoveY = absoluteY - relative.Y * Scale;
                    GetComparedImage();
                    break;
                }


            }
        }
        private void GetComparedImage()
        {
            var borders = Children.OfType<Border>().ToArray();
            var t = (Border)borders[0].Child;
            //t.La();
            RenderTargetBitmap rtb = new RenderTargetBitmap(new PixelSize((int)Math.Max(t.Bounds.Width, 1), Math.Max((int)t.Bounds.Width, 1)), new Vector(96, 96));
            rtb.Render(t);
            var t2 = (Grid)borders[1].Child;
            //t2.UpdateLayout();
            RenderTargetBitmap rtb2 = new RenderTargetBitmap(new PixelSize((int)Math.Max(t2.Bounds.Width, 1), Math.Max((int)t2.Bounds.Width, 1)), new Vector(96, 96));
            rtb2.Render(t2);

            ComparisonImage = CompareImages(rtb, rtb2);
            ForceUpdate = false;
            //PngBitmapEncoder png2 = new PngBitmapEncoder();
            //png2.Frames.Add(BitmapFrame.Create(rtb2));
            //FileStream stream2 = new FileStream("temp/test2.png", FileMode.Create);
            //png2.Save(stream2);
            //stream2.Close();
        }
        public static WriteableBitmap ConvertToWriteableBitmap(RenderTargetBitmap renderTargetBitmap)
        {
            var memoryStream = new MemoryStream();
            renderTargetBitmap.Save(memoryStream);
            return WriteableBitmap.Decode(memoryStream);
        }


        private WriteableBitmap CompareImages(RenderTargetBitmap bt1, RenderTargetBitmap bt2)
        {
            //WriteableBitmap wb1 = ConvertToWriteableBitmap(bt1);
            //WriteableBitmap wb2 = ConvertToWriteableBitmap(bt2);

            WriteableBitmap result = new WriteableBitmap(new PixelSize((int)bt1.Size.Width, (int)bt1.Size.Height), new Vector(96, 96));
            

            /*wb1.Lock();

            var buffer_new = new byte[(int)bt1.Size.Width * (int)bt1.Size.Height * 4];


            using (var fb1 = wb1.Lock())
            using (var fb2 = wb2.Lock())
            using (var fbres = result.Lock())
            {
                unsafe
                {
                    for (int y = 0; y < (int)bt1.Size.Height; y++)
                    {
                        var row1 = (uint*)(fb1.Address + y * fb1.RowBytes);
                        var row2 = (uint*)(fb2.Address + y * fb2.RowBytes);
                        var rowresult = (uint*)(fbres.Address + y * fbres.RowBytes);

                        for (int x = 0; x < (int)bt1.Size.Width; x++)
                        {
                            // Access the pixel at (x, y)
                            var pixel1 = row1[x];
                            var alpha1 = (byte)(pixel1 >> 24);
                            var red1 = (byte)(pixel1 >> 16);
                            var green1 = (byte)(pixel1 >> 8);
                            var blue1 = (byte)(pixel1);

                            var pixel2 = row2[x];
                            var alpha2 = (byte)(pixel2 >> 24);
                            var red2 = (byte)(pixel2 >> 16);
                            var green2 = (byte)(pixel2 >> 8);
                            var blue2 = (byte)(pixel2);

                            var diff = Math.Sqrt(Math.Pow(red2 - red1, 2) +
                                Math.Pow(green2 - green1, 2) +
                                Math.Pow(blue2 - blue1, 2)
                                + Math.Pow(alpha2 - alpha1, 2)) * 0.5;
                            if (diff > 5)
                            {
                                diff += 20;
                                var val = clip(diff);
                                var col = clip(diff * 0 / 255.0 + 0.5);
                                var red = clip(diff * diff / 255.0 + 0.5);
                                rowresult[x] = ((uint)col)
                                    | ((uint)col << 8) 
                                    | ((uint)val << 8)
                                    | ((uint)val << 8);
                            }
                        }
                    }
                }
            }*/
            return result;
        }
        public byte clip(double i)
        {
            if (i > 255) return 255;
            if (i < 0) return 0;
            else return (byte)i;
        }

    }
}
