# Tester for SVG to XAML conversion in Inkscape

I'm in the process of rewriting the XAML export/import in Inkscape. Therefore, I needed a testing tool that allows me to quickly check my conversion routine for errors. 

### What does it do?

This project scans a selected folder for SVG and XAML files, allows the user to select one, converts it in the background, compares them and allows to quickly zoom an pan inside the file. Any stdout output from both the PNG rendering and the XAML exporting process is displayed. `

### How does it look like?

![](screenshot.png)

The exported image on the right stems from the old exporting method (XSLT based from > 10 years ago).

### How does it work?

The input SVG is exported as PNG using Inkscape and displayed (I didn't want to bring the potential errors of a third party renderer package into the equation).

Since XAML is primarily used in WPF (maybe I'll add an export method for Android Studio though), the rendering of the processed XAML is handled in WPF directly.

### Future improvements

The utility can be easily adapted to other conversion paths, provided that there is a way to render the output in WPF. (Making the conversion path user-configurable is out of scope, though.) Fixing EMF import/export of Inkscape could be a future project.