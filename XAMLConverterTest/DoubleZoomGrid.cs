﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace XAMLConverterTest
{
    public class DoubleZoomGrid : Grid
    {


        public BitmapSource ComparisonImage
        {
            get { return (BitmapSource)GetValue(ComparisonImageProperty); }
            set { SetValue(ComparisonImageProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ComparisonImage.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ComparisonImageProperty =
            DependencyProperty.Register("ComparisonImage", typeof(BitmapSource), typeof(DoubleZoomGrid), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));



        public double Scale
        {
            get { return (double)GetValue(ScaleProperty); }
            set { SetValue(ScaleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Scale.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ScaleProperty =
            DependencyProperty.Register("Scale", typeof(double), typeof(DoubleZoomGrid), new FrameworkPropertyMetadata(1.0, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));



        public double MoveX
        {
            get { return (double)GetValue(MoveXProperty); }
            set { SetValue(MoveXProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MoveX.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MoveXProperty =
            DependencyProperty.Register("MoveX", typeof(double), typeof(DoubleZoomGrid), new FrameworkPropertyMetadata(0.0, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));



        public double MoveY
        {
            get { return (double)GetValue(MoveYProperty); }
            set { SetValue(MoveYProperty, value); }
        }

        public object BitmapFactory { get; private set; }

        // Using a DependencyProperty as the backing store for MoveY.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MoveYProperty =
            DependencyProperty.Register("MoveY", typeof(double), typeof(DoubleZoomGrid), new FrameworkPropertyMetadata(0.0, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));


        // super ugly, forces the view to update itself after the render / conversion process finished
        public bool ForceUpdate
        {
            get { return (bool)GetValue(ForceUpdateProperty); }
            set { SetValue(ForceUpdateProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ForceUpdate.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ForceUpdateProperty =
            DependencyProperty.Register("ForceUpdate", typeof(bool), typeof(DoubleZoomGrid), new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault,
                (a, b) =>
                {
                    if ((bool)b.NewValue)
                    {
                        ((DoubleZoomGrid)a).GetComparedImage();
                    }
                }));



        private Point start;
        private Point origin;

        public override void EndInit()
        {
            this.MouseWheel += DoubleZoomGrid_MouseWheel;
            this.MouseMove += DoubleZoomGrid_MouseMove;
            this.MouseLeftButtonUp += DoubleZoomGrid_MouseLeftButtonUp;
            this.MouseLeftButtonDown += DoubleZoomGrid_MouseLeftButtonDown;
            
        }
        public void RefreshComparison()
        {
            GetComparedImage();
        }

        private void DoubleZoomGrid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            foreach (var child in Children.OfType<Border>())
            {
                if (child.Child == null) continue;
                if (child.Child.IsMouseOver)
                {
                    start = e.GetPosition(this);
                    origin = new Point(MoveX, MoveY);
                    this.Cursor = Cursors.Hand;
                    this.CaptureMouse();

                }
            }
        }

        private void DoubleZoomGrid_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            this.ReleaseMouseCapture();
            this.Cursor = Cursors.Arrow;
        }

        private void DoubleZoomGrid_MouseMove(object sender, MouseEventArgs e)
        {
            if (this.IsMouseCaptured)
            {
                Vector v = start - e.GetPosition(this);
                MoveX = origin.X - v.X;
                MoveY = origin.Y - v.Y;
                GetComparedImage();
            }
        }

        private void DoubleZoomGrid_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            double zoom = e.Delta > 0 ? 1.1 : 1d / (1.1);
            if (!(e.Delta > 0) && (Scale < .4 || Scale < .4))
                return;

            foreach (var child in Children.OfType<Border>())
            {
                if (child.Child == null) continue;
                if (child.Child.IsMouseOver)
                {
                    Point relative = e.GetPosition(child.Child);
                    double absoluteX;
                    double absoluteY;

                    absoluteX = relative.X * Scale + MoveX;
                    absoluteY = relative.Y * Scale + MoveY;

                    Scale *= zoom;

                    MoveX = absoluteX - relative.X * Scale;
                    MoveY = absoluteY - relative.Y * Scale;
                    GetComparedImage();
                    break;
                }


            }
        }
        private void GetComparedImage()
        {
            var borders = Children.OfType<Border>().ToArray();
            var t = (Border)borders[0].Child;
            t.UpdateLayout();
            RenderTargetBitmap rtb = new RenderTargetBitmap((int)t.ActualWidth, (int)t.ActualHeight, 96, 96, PixelFormats.Pbgra32);
            rtb.Render(t);
            var t2 = (Grid)borders[1].Child;
            t2.UpdateLayout();
            RenderTargetBitmap rtb2 = new RenderTargetBitmap((int)t2.ActualWidth, (int)t2.ActualHeight, 96, 96, PixelFormats.Pbgra32);
            rtb2.Render(t2);

            ComparisonImage = CompareImages(rtb, rtb2);
            ForceUpdate = false;
            //PngBitmapEncoder png2 = new PngBitmapEncoder();
            //png2.Frames.Add(BitmapFrame.Create(rtb2));
            //FileStream stream2 = new FileStream("temp/test2.png", FileMode.Create);
            //png2.Save(stream2);
            //stream2.Close();
        }
        private BitmapSource CompareImages(BitmapSource bt1, BitmapSource bt2)
        {
            WriteableBitmap wb1 = new WriteableBitmap(bt1);
            WriteableBitmap wb2 = new WriteableBitmap(bt2);

            var buffer1 = new byte[bt1.PixelWidth * bt1.PixelHeight * 4];
            wb1.CopyPixels(buffer1, bt1.PixelWidth * 4, 0);

            var buffer2 = new byte[bt2.PixelWidth * bt2.PixelHeight * 4];
            wb2.CopyPixels(buffer2, bt2.PixelWidth * 4, 0);

            var buffer_new = new byte[buffer2.Length];
            var r = new Random();
            for (int x = 0; x < bt2.PixelWidth; x++)
            {
                for (int y = 0; y < bt2.PixelHeight; y++)
                {
                    int left_index = 4 * (x + y * bt1.PixelWidth);
                    int right_index = 4 * (x + y * bt2.PixelWidth);

                    if (left_index >= buffer1.Length) continue;

                    var diff = Math.Sqrt(Math.Pow(buffer2[right_index] - buffer1[left_index], 2) +
                        Math.Pow(buffer2[right_index + 1] - buffer1[left_index + 1], 2) +
                        Math.Pow(buffer2[right_index + 2] - buffer1[left_index + 2], 2)) * 0.5;
                    if (diff > 5)
                    {
                        diff += 20;
                        var val = clip(diff) ;
                        var col = clip(diff * 0 / 255.0 + 0.5);
                        var red = clip(diff * diff / 255.0 + 0.5);
                        buffer_new[right_index] = col;
                        buffer_new[right_index + 1] = col;
                        buffer_new[right_index + 2] = val;
                        buffer_new[right_index + 3] = val;
                    }
                }
            }
            return ByteArrayToImage(buffer_new, bt2.PixelWidth, bt2.PixelHeight);
        }
        public byte clip(double i)
        {
            if (i > 255) return 255;
            if (i < 0) return 0;
            else return (byte)i;
        }
        public BitmapSource ByteArrayToImage(byte[] BArray, int width, int height)
        {
            var dpiX = 96d;
            var dpiY = 96d;
            var pixelFormat = PixelFormats.Pbgra32;
            var bytesPerPixel = (pixelFormat.BitsPerPixel + 7) / 8;
            var stride = bytesPerPixel * width;

            var bitmap = BitmapImage.Create(width, height, dpiX, dpiY, pixelFormat, null, BArray, stride);
            return bitmap;
        }

    }
}
