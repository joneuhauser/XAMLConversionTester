﻿using System;
using System.Globalization;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Markup;
using System.Windows.Media;

namespace XAMLConverterTest
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            if (Directory.Exists("temp"))
            {
                Directory.Delete("temp", true);
                // "fix" for race condition, sometimes the folder is deleted and not recreated
                Thread.Sleep(100);
            }
            
            Directory.CreateDirectory("temp");
            InitializeComponent();
            var vm =  new MainWindowViewModel();
            MainGrid.DataContext = vm;
            // clear the temporary files at launch
            
        }

        private void tb_XamlString_TargetUpdated(object sender, DataTransferEventArgs e)
        {
            MainWindowViewModel vm = content.DataContext as MainWindowViewModel;
            try
            {
                content.Children.Clear();
                var rootObject = XamlReader.Parse(vm.XAMLData);

                if (rootObject is Grid g)
                {
                    if (g.Resources.Contains("tempDrawingImage"))
                    {
                        if (g.Resources["tempDrawingImage"] is DrawingGroup dg)
                        {
                            if (dg.Children[0] is DrawingGroup dg2)
                            {
                                if (dg2.ClipGeometry != null)
                                {
                                    var bounds = dg2.ClipGeometry.Bounds;
                                    dg2.Children.Add(new GeometryDrawing(null, new Pen(Brushes.Black, 0.001), new RectangleGeometry(bounds)));
                                }
                            }
                        }
                    }
                }
                if (rootObject is DrawingGroup dg3)
                {
                    addDrawingGroup(dg3);
                }
                
                if (rootObject is ResourceDictionary d)
                {
                    foreach (object key in d.Keys)
                    {
                        if (d[key] is DrawingGroup dg2)
                        {
                            addDrawingGroup(dg2);
                        }
                        if (d[key] is Canvas c2)
                        {
                            content.Children.Add(c2);
                        }
                        if (d[key] is Viewbox w)
                        {
                            content.Children.Add(w);
                        }
                        if (d[key] is SolidColorBrush b)
                        {
                            if (content.Resources.Contains(key))
                                content.Resources.Remove(key);
                            content.Resources.Add(key, b);
                        }
                    }
                }
                if (rootObject is Viewbox c)
                {
                    content.Children.Add(c);
                }
                

            }
            catch (XamlParseException ex)
            {
                vm.OutputProcessingText += "\nError processing file: \n" + ex;
            }
            catch (ArgumentNullException) { }
        }
        void addDrawingGroup(DrawingGroup dg2)
        {
            if (dg2.ClipGeometry == null)
            {
                if (dg2.Children.Count == 1 && dg2.Children[0] is DrawingGroup dg3 && dg3.ClipGeometry != null)
                {
                    addDrawingGroup(dg3);
                    return;
                }
            }
            if (dg2.ClipGeometry != null)
            {
                var bounds = dg2.ClipGeometry.Bounds;
                dg2.Children.Add(new GeometryDrawing(null, new Pen(Brushes.Black, 0.001), new RectangleGeometry(bounds)));
            }
            DrawingImage di = new DrawingImage();
            di.Drawing = dg2;
            Image im = new Image();
            im.Source = di;
            //content.Resources = d;
            content.Children.Add(im);
        }
    }

    public class PathToFilenameConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Path.GetFileName(value.ToString());
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
