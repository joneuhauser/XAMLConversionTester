﻿using DynamicExpresso;
using Microsoft.WindowsAPICodePack.Dialogs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Media.Imaging;
using XAMLConverterTest.Properties;

namespace XAMLConverterTest
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }


        public string SourceFolder
        {
            get
            {
                string fn = Properties.Settings.Default.Filename;
                return Directory.Exists(fn) ? fn : Path.GetDirectoryName(fn);
            }
            set
            {
                reloadFiles(value);
                OnPropertyChanged();
            }
        }

        private ObservableCollection<string> _filesList;

        public ObservableCollection<string> FilesList
        {
            get { return _filesList; }
            set { _filesList = value; OnPropertyChanged(); }
        }

        public string CurrentFileName
        {
            get { return FilesList[CurrentIndex]; }
        }

        private int _currentIndex;

        public int CurrentIndex
        {
            get { return _currentIndex; }
            set
            {
                _currentIndex = value;
                if (value < FilesList.Count && value >= 0)
                {
                    Properties.Settings.Default.Filename = FilesList[value];
                    Properties.Settings.Default.Save();
                    ReadFile();
                    OnPropertyChanged();
                    OnPropertyChanged("CurrentFileName");
                }
            }
        }

        private BitmapImage _originalImage;
        public BitmapImage OriginalImage
        {
            get
            {
                return _originalImage;
            }
            set
            {
                _originalImage = value;
                OnPropertyChanged();
            }
        }
        private string xamlData;

        public string XAMLData
        {
            get { return xamlData; }
            set { xamlData = value; OnPropertyChanged(); }
        }

        private double scale = 1;

        public double Scale
        {
            get { return scale; }
            set
            {
                scale = value;
                if (scale / (currentSVGSize / 1000) > 3 && (int)(scale * 1000) < 20000)
                    DrawSVG(CurrentFileName, (int)(scale * 1000));
                OnPropertyChanged();
            }
        }
        private double opacity = 0.5;

        public double Opacity
        {
            get { return opacity; }
            set { opacity = value; OnPropertyChanged();  }
        }

        private double moveX;

        public double MoveX
        {
            get { return moveX; }
            set { moveX = value; OnPropertyChanged(); }
        }

        private double moveY;

        public double MoveY
        {
            get { return moveY; }
            set { moveY = value; OnPropertyChanged(); }
        }
        private string inputProcessingText;

        public string InputProcessingText
        {
            get { return inputProcessingText; }
            set { inputProcessingText = value; OnPropertyChanged(); }
        }

        private string outputProcessingText;

        public string OutputProcessingText
        {
            get { return outputProcessingText; }
            set { outputProcessingText = value; OnPropertyChanged(); }
        }

        private bool outputRefreshing;

        public bool OutputRefreshing
        {
            get { return outputRefreshing; }
            set { outputRefreshing = value; OnPropertyChanged(); }
        }
        private bool inputRefreshing;

        public bool InputRefreshing
        {
            get { return inputRefreshing; }
            set { inputRefreshing = value; OnPropertyChanged(); }
        }


        private bool forceUpdate;

        public bool ForceUpdate
        {
            get { return forceUpdate; }
            set { forceUpdate = value; OnPropertyChanged(); }
        }

        private ObservableCollection<string> interpreters;

        public ObservableCollection<string> Interpreters
        {
            get { return interpreters; }
            set { interpreters = value; OnPropertyChanged(); }
        }

        private int interpreterIndex;

        public int InterpreterIndex
        {
            get { return interpreterIndex; }
            set { interpreterIndex = value; OnPropertyChanged(); if (FilesList?.Count > 0) ConvertSVG(CurrentFileName); }
        }



        #region command defs
        private RelayCommand _openCommand;

        public RelayCommand OpenCommand
        {
            get { return _openCommand; }
            set { _openCommand = value; OnPropertyChanged(); }
        }
        private RelayCommand _forwardCommand;

        public RelayCommand ForwardCommand
        {
            get { return _forwardCommand; }
            set { _forwardCommand = value; OnPropertyChanged(); }
        }
        private RelayCommand _backwardsCommand;

        public RelayCommand BackwardsCommand
        {
            get { return _backwardsCommand; }
            set { _backwardsCommand = value; OnPropertyChanged(); }
        }
        private RelayCommand _refreshCommand;

        public RelayCommand RefreshCommand
        {
            get { return _refreshCommand; }
            set { _refreshCommand = value; OnPropertyChanged(); }
        }

        private RelayCommand _resetViewCommand;

        public RelayCommand ResetViewCommand
        {
            get { return _resetViewCommand; }
            set { _resetViewCommand = value; OnPropertyChanged(); }
        }

        private RelayCommand _openFileCommand;

        public RelayCommand OpenFileCommand
        {
            get { return _openFileCommand; }
            set { _openFileCommand = value; }
        }

        private int currentSVGSize = 1000;

        #endregion
        public MainWindowViewModel()
        {
            Settings.Default.Upgrade();
            OpenCommand = new RelayCommand((o) =>
            {
                CommonOpenFileDialog ofd = new CommonOpenFileDialog();
                ofd.InitialDirectory = SourceFolder;
                ofd.IsFolderPicker = true;
                if (ofd.ShowDialog() == CommonFileDialogResult.Ok)
                {
                    SourceFolder = ofd.FileName;
                }
            });
            ForwardCommand = new RelayCommand((o) => CurrentIndex++, (o) => CurrentIndex < FilesList.Count());
            BackwardsCommand = new RelayCommand((o) => CurrentIndex--, (o) => CurrentIndex > 0);
            RefreshCommand = new RelayCommand((o) => reloadFiles());
            ResetViewCommand = new RelayCommand((o) => {
                ResetView();
                ForceUpdate = true;
            });
            OpenFileCommand = new RelayCommand((o) => OpenConvertedFile());

            Interpreters = new ObservableCollection<string>(Settings.Default.InterpreterPaths.Cast<string>().ToList());
            InterpreterIndex = 0;
            reloadFiles();
        }
        private void ResetView()
        {
            Scale = 1; MoveX = 0; MoveY = 0;
        }

        private void ReadFile()
        {
            OutputProcessingText = "";
            InputProcessingText = "";
            DrawSVG(CurrentFileName);


            ConvertSVG(CurrentFileName);
        }
        private BitmapImage ReadBitmap(string path)
        {
            BitmapImage src = new BitmapImage();
            if (File.Exists(path))
            {

                src.CreateOptions = BitmapCreateOptions.IgnoreImageCache;
                src.BeginInit();
                src.UriSource = new Uri(path, UriKind.Relative);
                src.CacheOption = BitmapCacheOption.OnLoad;
                src.EndInit();

            }
            else
            {
                OutputProcessingText += "File not found";
            }
            return src;
        }
        private string DrawSVG(string filename, int size = 0)
        {
            if (size == 0)
            {
                size = 1000;
            }
            InputRefreshing = true;
            string outfn = Path.Combine("temp", Path.GetFileNameWithoutExtension(filename) + "_in_" + DateTime.Now.Ticks + ".png");
            RunProcess(Properties.Settings.Default.InkscapePath,
                "--export-type=\"png\" --export-filename=\"" + outfn + "\"" + " --export-overwrite --export-width=" + size + " --export-background=#00000000 --export-background-opacity=1 \"" + filename + "\"",
                new Action<string>((s) => {                    InputProcessingText += s;
                }),
                () => Application.Current.Dispatcher.Invoke(() =>
                {
                    OriginalImage = ReadBitmap(outfn);
                    InputRefreshing = false;
                    ForceUpdate = true;
                }), 0);
            currentSVGSize = size;
            return outfn;
        }
        private string outfn;
        private void ConvertSVG(string filename)
        {
            StopWatcher();
            outfn = Path.Combine("temp", Path.GetFileNameWithoutExtension(filename) + "_in_" + DateTime.Now.Ticks + ".xaml");
            OutputRefreshing = true;
            string path = Interpreters[InterpreterIndex];
            string arguments = Settings.Default.InterpreterArguments[InterpreterIndex];
            arguments = arguments.Replace("%infn%", filename).Replace("%outfn%", outfn);
            RunProcess(path, arguments,
                new Action<string>((s) =>
                {
                    OutputProcessingText += s;
                }), () => { DrawXAML(outfn); StartFileSystemWatcher(outfn); }, 1);
        }

        private void StopWatcher()
        {
            if (watcher != null)
            {
                watcher.Changed -= OnChanged;
            }
        }

        FileSystemWatcher watcher;
        private void StartFileSystemWatcher(string filename)
        {
            watcher = new FileSystemWatcher();
            watcher.Path = Path.GetDirectoryName(filename);
            watcher.NotifyFilter = NotifyFilters.LastWrite;
            watcher.Filter = Path.GetFileName(filename);
            watcher.Changed += OnChanged;
            watcher.EnableRaisingEvents = true;
        }

        private void OnChanged(object sender, FileSystemEventArgs e)
        {
            Thread.Sleep(100);
            DrawXAML(e.FullPath);   
        }

        private void DrawXAML(string filename)
        {
            OutputRefreshing = true;
            if (File.Exists(filename))
            {
                try
                {
                    StreamReader sr = new StreamReader(filename);
                    XAMLData = sr.ReadToEnd();
                    sr.Close();


                    ResetView();
                    OutputProcessingText += "\nDrawing complete\n";
                }
                catch (IOException ex)
                {
                    OutputProcessingText += "\nCan't read file\n";
                }
            }
            else
            {
                OutputProcessingText += "\nFile not found\n";
            }
            OutputRefreshing = false;
            ForceUpdate = true;

        }
        private void OpenConvertedFile()
        {
            RunProcess(Properties.Settings.Default.EditorPath, "\"" + outfn + "\"", (o) => { }, () => { }, 2);
        }

        private void reloadFiles(string fn = null)
        {
            if (fn == null)
                fn = SourceFolder;
            FilesList = new ObservableCollection<string>(Directory.EnumerateFiles(fn, "*.xaml").Concat(Directory.EnumerateFiles(fn, "*.svg")).OrderBy(x => x));
            fn = Properties.Settings.Default.Filename;
            int tentative_index = FilesList.IndexOf(fn);
            if (File.Exists(fn) && tentative_index != -1)
                CurrentIndex = tentative_index;
            else
                CurrentIndex = 0;
        }
        private Process[] process = new Process[3];
        private void RunProcess(string fileName, string args, Action<string> outputProcessing, Action endCallback, int index)
        {
            if (index < 2)
            {
                // kill old process
                process[index]?.Kill();
            }
            process[index] = new Process
            {
                StartInfo =
                {
                    FileName = fileName, Arguments = args,
                    UseShellExecute = false, CreateNoWindow = true,
                    RedirectStandardOutput = true, RedirectStandardError = true
                },
                EnableRaisingEvents = true
            };
            outputProcessing(process[index].StartInfo.FileName + " " + process[index].StartInfo.Arguments + "\n");
            process[index].Exited += new EventHandler((o, ea) => {
                try
                {


                    outputProcessing("Process exited");
                    endCallback();
                    process[index] = null;
                }
                catch
                {}
            });
            var dea = new DataReceivedEventHandler((o, ea) =>
            {
                if (ea.Data != null)
                {
                    outputProcessing(ea.Data + "\n");
                }
            });
            process[index].OutputDataReceived += dea;
            process[index].ErrorDataReceived += dea;

            process[index].Start();
            process[index].BeginOutputReadLine();
            process[index].BeginErrorReadLine();
        }
        
    }
}
